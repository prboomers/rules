import { defineUserConfig } from "vuepress-vite";
import type { DefaultThemeOptions, ViteBundlerOptions } from "vuepress-vite";

export default defineUserConfig<DefaultThemeOptions, ViteBundlerOptions>({
    lang: "en-UK",
    title: "PR Boomer Community Rules",
    description: "A website for the PR Boomers server and community rules.",

    base: process.env.NODE_ENV == "production" ? "/rules/" : "/",
    bundler: "@vuepress/vite",
    theme: "@vuepress/default",
    themeConfig: {
        lastUpdated: true,
        contributors: false,

        // logo: "TODO Boomer Logo",

        // Navbar configuration
        navbar: [
            {
                text: "Discord",
                link: "https://discord.com/channels/793477012257112104/824312960833945610",
            },
        ],
        notFound: ["There seems to be nothing here"],
    },
    markdown: {
        anchor: {
            permalink: true,
        },
    },
});
