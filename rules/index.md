---
lang: en-UK
title: Server Rules
description: Rules for the project reality server.
---

# Server Rules

## Conduct

1.  The server's language is English.
    Use English when speaking to players not in your squad.
1.  Racist, explicit, extremist or otherwise clearly offensive content is prohibited in any form, both in discord or in-game.
    This includes chat messages, player- or squad names, or audio broadcast through mumble.
1.  Falsely reporting players, or lying to the admins is strictly forbidden.
1.  Attempting to deceive others of your identity or impersonating others is strictly forbidden.
1.  Live streaming on the server is prohibited unless otherwise stated by the admins on a per case basis.

## Communication

1.  Having a working microphone and using mumble is mandatory.
1.  Squad leaders must be responsive on squad leader mumble.
1.  Use direct squad leader channels `Numpad 1–9` whenever possible.
1.  Spamming in mumble or chat is forbidden.
    Playing music outside of squad channels is forbidden.
    Do not pad chat messages with spaces to make them appear closer to the middle of the screen.

## Gameplay

1.  Cheating, ghosting, glitching or otherwise abusing the game for an unfair advantage is forbidden.
1.  Griefing, revenge team killing, or otherwise distrupting gameplay is forbidden.
1.  Squad leaders are responsible for organizing their squads and making sure their members contribute to their teams' efforts.
1.  Hijacking squads is forbidden.
    If a squad leader resigns on accident, by disconnecting, or for gameplay reasons, do not attempt to take over their squad without permission.

### Infantry and kits

1.  Free kit squads are forbidden.
1.  Stealing kits is forbidden.
    Respect downed players' requests with regards to their kits.
    If they plan on getting revived or respawing and recovering their kit, do not attempt to claim their kits as your own.
1.  Joining a squad for the sole purpose of taking a limited kit and then leaving is considered stealing.
1.  Every non-asset squad is an infantry squad.
1.  Infantry squads may be locked with 6 or more members.

### Vehicles and assets

1.  Intentional roadkilling is forbidden.
1.  Ramming vehicles if forbidden.
    VBIEDs may ram enemy vehicles with the intent to detonate in close proximity.
1.  Appropriately named squads may claim a category of assets each.
    Claims apply in the order of squad creation. See the full list of asset categories [here](#asset-categories).
1.  Miscellaneous vehicles, not claimable by a named squad, may still be claimed on a _first come first served_ basis.
1.  Avoid hoarding assets.
    Let others make use of vehicles you do not need.
1.  Ensure each vehicle entering combat is sufficiently crewed.
1.  Stealing assets claimed by other squads is forbidden.
    #### Additional rules apply to air assets, air combat, and anti-aircraft weapons:
    1.  Pilots must be skilled enough to fly without excessively crashing or wasting their aircraft.
    1.  Camping the enemy main with anti-aircraft assets or weapons is forbidden.
        Do not fire at aircraft landing or taking off over their main base.
    1.  Starting an air-to-air engagement within viewrange of your target's main base is forbidden.
    1.  Ongoing air-to-air engagements may continue, even if a participant retreats to their main base, as long as they do not manage to land within their base.

### Special protection rules

#### Main bases

1.  Main bases may not be fired upon or attacked in any way unless they contain a flag which is in play (indicated by an AAS marker).
1.  Firing into the the _dome of death_ (DOD) surrounding main bases is not allowed, unless the corresponding team's last flag is in play, or a mapper defined objective or the actions of the enemy team otherwise force you to fight in close proximity of DOD.

#### First flags

1.  Rushing the enemy team's first capturable flag is prohibited.

    ##### Actions considered rushing include:

    1.  Attacking people inside the radius of the flag.
    1.  Preventing people from entering the radius of the flag.
    1.  Entering the radius of the flag.

    ##### Rush protection does not apply, once one of these conditions is fulfulled:

    1.  10 minutes of in-game time have passed.
    1.  The flag is capturable by the attacking team.
    1.  The flag was captured by the defending team.
    1.  There are only two neutral flags at the start of the round.
    1.  The attacking team has paradrop spawns at the start of the round.

#### Seeding

1.  Flags that cause the enemy team to bleed ticktes should not be attacked while seeding.
1.  FOBs should not be destroyed or camped while seeding.
1.  Claimable assets should not be used until there are at least 40 players on the server, or an active admin authorizes otherwise.

## Appendix

### Asset squad naming

1.  Claim assets by naming your squad something that contains exactly one of the [asset category names](#asset-categories), such as `TANK`, or `TANK-TOP`.
2.  Squads attempting to claim multiple categories, such as `CAS TANK` are not allowed.
3.  Synonyms such as `ARMOR`, vehicle names like `Challenger`, or misspelled versions of the words such as `T4NK` or `TONK` can not be used to claim assets.

### Asset categories

#### `TANK`

1.  Holds claims if the team is provided with at least one tank.
1.  Claims
    - tanks, including main battle tanks, and scout tanks,
    - ATGM vehicles,
    - SPG- and rocket artillery technicals,
    - anti-aircraft vehicles.
1.  Locking this squad is not allowed until all tanks are adequately crewed.

#### `CAS`

1.  Holds claims if the team is provided with at least one attack aicraft.
1.  Claims
    - all attack aicraft, including jets, planes or helicopters
    - anti-aircraft vehicles not used by a `TANK` squad.
1.  Locking this squad is not allowed until all attack aicrcraft are adequately crewed.

#### `APC`

1.  Claims
    - APCs and IFVs that require two crewmen to be operated,
    - reconnaissance vehicles, such as BRDM-2s , VN-3s, and Fenneks,
    - ATGM vehicles
    - SPG- and rocket artillery technicals not used by a `TANK` squad,
    - anti-aircraft vehicles not used by a `CAS` or `TANK` squad,
    - light APCs without a turrent, if your team is provided no other kind of vehicle claimable by the squad.
1.  Does _not_ claim:
    - jeeps or MRAPs with remote weapon systems.
    - light APCs without a turret, if your team is provided at least one other other vehicle that fits in the claimed categories.
1.  Locking this squad is not allowed until all vehicles directly at your disposal, not claimable by a `CAS` or `TANK` squad, are adequately crewed.

#### `TRANS` or `TRANSPORT`

1.  Claims all transport aircraft.
1.  This squad may be locked once the team's transportation needs are reasonably fulfilled, or all transport aircraft are manned.

#### `MORTAR`

1.  Claims all mortar emplacements.
1.  Locking this squad is not allowed below 3 members.
