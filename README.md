# PR Boomer Rules

> This is a page for maintainers and developers.
> If you are looking for the rules themseves, see https://prboomers.gitlab.io/rules/


## Rule changes

To make rule changes easy to followg and git friendly, put each rule, and each sentence of the rule on it's own line.

Commits which change the rules should be prefixed with `Rule:`, and include a description of what changed.

To update the rules website, push a new tag to `master`.
The tag should be a version number following [semantic versioning for documents](https://semverdoc.org/).

```sh
# see previous tags
> git tag -l
...
v1.2.3 # last version was v1.2.3

# for formatting or typo fix changes bump the patch version
> git tag -a v1.2.4

# if your changes affect the meaning of the rules, bump the minor version
> git tag -a v1.3.0

# if your changes change an entire section or more in the document, bump the major version
> git tag -a v2.0.0

# don't forget to push your tag with the commit
> git push --follow-tags
```

Provide a meaningful list of changes in the tag message whenever appropriate.

Once your newly created tag is pushed, Gitlab CI automatically updates the rules.
It might take a few minutes for changes to appear on the rules page.

## Development

The rules are hosted on a static [Vuepress](https://vuepress2.netlify.app/) site.
You will require [node](https://nodejs.org/en/) and [yarn](https://yarnpkg.com/) for local development.
The website is statically generated from the markdown files found within the [`rules`](rules) folder.

```sh
# install yarn using npm if you don't have it already
> npm install -g yarn

# install dependencies
> yarn

# host a local development server with HMR using Vite
> yarn dev
```
